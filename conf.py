# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import re
import sys


sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------
html_title = "Project Title"
copyright = '2022, Tvillingfabrikken AS'
author = 'Even Falkenberg Langås'

# # The full version, including alpha/beta/rc tags
version = re.sub('^v', '', os.popen('git describe').read().strip())

# The full version, including alpha/beta/rc tags
# release = 'https://gitlab.com/pages/sphinx'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # 'furo'
    # 'recommonmark',
    # 'sphinx_rtd_theme',
    # 'sphinxcontrib.bibtex'
]


# Handle both ReST and Markdown files
# source_suffix = {
#     '.rst': 'restructuredtext',
#     '.txt': 'markdown',
#     '.md': 'markdown'
# }

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".

html_logo = "./src/figures/tvillingfabrikkenLogoDarkRing.png"
html_favicon = "./src/figures/tvillingfabrikkenLogoDarkRing.png"

numfig = True

numfig_format = {
    'figure': 'Figure %s.'
    }

numfig_secnum_depth = 1

html_theme_options = {
    "light_css_variables": {
        "admonition-font-size": '0.92rem'
    }
}