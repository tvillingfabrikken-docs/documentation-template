.. master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Documentation Template
===================================================

This paragraph can be used as a short introduction to what the document is about. When making documentation for Tvillingfabrikken's products, this template should be used.

**Authors:** Even Falkenberg Langås


.. toctree::
   :numbered: 2
   :maxdepth: 2
   :caption: Contents

   src/chapter1.rst
   src/chapter2.rst

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
